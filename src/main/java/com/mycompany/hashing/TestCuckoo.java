/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.hashing;

import java.util.Map;

/**
 *
 * @author Aritsu
 */
public class TestCuckoo {

    public static void main(String[] args) {
        Map<Integer, String> map = new CuckooHashMap<Integer, String>();
        int itemCount = 25;
        for (int i = 0; i < itemCount; i++) {
            Integer key = i;
            String val = "Value_" + i;
            map.put(key, val);
        }

        System.out.println(map.get(1));

        for (String v : map.values()) {
            System.out.println(v);
        }
    }
}
