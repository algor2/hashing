/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.hashing;

/**
 *
 * @author Aritsu
 */
public class Hashing<Key, Value> {

    private int M = 100;
    private Key[] keys = (Key[]) new Object[M];
    private Value[] vals = (Value[]) new Object[M];

    private int hash(Key key) {
        char[] ascii = key.toString().toCharArray();
        int sum = 0;
        for (int i = 0; i < ascii.length; i++) {
            sum += ascii[i];
        }
        return (sum) % M;
    }

    public void put(Key key, Value val) {
        int i;
        for (i = hash(key); keys[i] != null; i = (i + 1) % M) {
            if (keys[i].equals(key)) {
                break;
            }
        }
        keys[i] = key;
        vals[i] = val;
    }

    public Value get(Key key) {
        for (int i = hash(key); keys[i] != null; i = (i + 1) % M) {
            if (key.equals(keys[i])) {
                return vals[i];
            }
        }
        return null;
    }

    public static void main(String[] args) {
        Hashing<String, Integer> hashing = new Hashing<>();
        String test = "eiei";
        hashing.put(test, 1);
        String find = "ei";
        
        if(hashing.get(find) == null){
            System.out.println("Not found");
        }else{
            System.out.println(hashing.get(find));
        }
    }

}
